#!/usr/bin/env bash

# validate required parameter
: CONFIG_ID=${CONFIG_ID:?'CONFIG_ID variable missing.'}

git config --global --add safe.directory "$BITBUCKET_CLONE_DIR"

cd "$BITBUCKET_CLONE_DIR"

git config http.${BITBUCKET_GIT_HTTP_ORIGIN}.proxy http://host.docker.internal:29418/

export CONFIGCAT_ALIAS_PATTERNS=${ALIAS_PATTERNS}

export CONFIGCAT_USAGE_PATTERNS=${USAGE_PATTERNS}

EXCL_KEYS="' '"
[ ! -z "${EXCLUDE_KEYS}" ] && EXCL_KEYS=${EXCLUDE_KEYS}

configcat scan "$BITBUCKET_CLONE_DIR/$SUB_FOLDER" \
    --config-id=${CONFIG_ID} \
    --repo=${BITBUCKET_REPO_SLUG} \
    --line-count=${LINE_COUNT} \
    --file-url-template="$BITBUCKET_GIT_HTTP_ORIGIN/src/{commitHash}/{filePath}#lines-{lineNumber}" \
    --commit-url-template="$BITBUCKET_GIT_HTTP_ORIGIN/commits/{commitHash}" \
    --runner="ConfigCat Bitbucket Pipe v1.8.0" \
    --upload \
    --verbose=${VERBOSE} \
    --non-interactive \
    --exclude-flag-keys ${EXCL_KEYS}