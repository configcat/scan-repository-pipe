# Bitbucket Pipelines Pipe:  ConfigCat Scan Repository

This pipe is a utility that discovers ConfigCat feature flag and setting usages in your source code and uploads the found code references to [ConfigCat](https://configcat.com). This feature makes the erasement of technical debt easier, as it can show which repositories reference your feature flags and settings in one centralized place on your [Dashboard](https://app.configcat.com).

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: configcat/scan-repository-pipe:1.8.0
  variables:
    CONFIG_ID: '<string>'
    # CONFIGCAT_API_HOST: '<string>'  # Optional
    # LINE_COUNT: '<integer>'  # Optional
    # SUB_FOLDER: '<string>'  # Optional
    # EXCLUDE_KEYS: '<string>'  # Optional
    # ALIAS_PATTERNS: '<string>'  # Optional
    # USAGE_PATTERNS: '<string>'  # Optional
    # VERBOSE: '<boolean>'  # Optional
```

## Variables

| Variable           | Usage                                                                                                                            |
| ------------------ | -------------------------------------------------------------------------------------------------------------------------------- |
| CONFIG_ID (\*)     | ID of the ConfigCat config to scan against. [More info](https://configcat.com/docs/advanced/code-references/overview#config-id). |
| CONFIGCAT_API_HOST | ConfigCat Management API host. Default: `api.configcat.com`.                                                                     |
| LINE_COUNT         | Context line count before and after the reference line. (min: 1, max: 10) Default: `4`.                                          |
| SUB_FOLDER         | Sub-folder to scan, relative to the repository root folder.                                                                      |
| EXCLUDE_KEYS       | List of feature flag keys that must be excluded from the scan report.                                                            |
| ALIAS_PATTERNS     | Comma delimited list of custom regex patterns used to search for additional aliases.                                             |
| USAGE_PATTERNS     | Comma delimited list of custom regex patterns that describe additional feature flag key usages.                                  |
| VERBOSE            | Turns on detailed logging. Default: `false`.                                                                                     |

_(\*) = required variable._

## Details

For more information about repository scanning, see our [documentation](https://configcat.com/docs/advanced/code-references/overview).

## Prerequisites

1. Create a new [ConfigCat Management API credential](https://app.configcat.com/my-account/public-api-credentials) and store its values in secure pipeline variables with the following names: `CONFIGCAT_API_USER`, `CONFIGCAT_API_PASS`.

2. [Get the ID of your ConfigCat Config](https://configcat.com/docs/advanced/code-references/overview#config-id) that you want to associate with your repository and use it in the `CONFIG_ID` variable. The scanner will use this ID to determine which feature flags and settings to search for in your source code.

## Examples

Basic example:

```yaml
- pipe: configcat/scan-repository-pipe:1.8.0
  variables:
    CONFIG_ID: 'YOUR-CONFIG-ID'
```

A bit more advanced example:

```yaml
- pipe: configcat/scan-repository-pipe:1.8.0
  variables:
    CONFIG_ID: 'YOUR-CONFIG-ID'
    LINE_COUNT: '3'
    SUB_FOLDER: '/src'
    EXCLUDE_KEYS: >
      flag_key_to_exclue_1
      flag_key_to_exclue_2
    ALIAS_PATTERNS: (\w+) = :CC_KEY,const (\w+) = feature_flags\.enabled\(:CC_KEY\)
    USAGE_PATTERNS: feature_flags\.enabled\(:CC_KEY\)
    VERBOSE: 'true'
```

## Support

If you would like help with this pipe, or you have an issue or feature request, [let us know](https://configcat.com/support).

If you are reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce