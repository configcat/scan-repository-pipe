FROM configcat/cli:2.4.0
RUN apk add --update --no-cache bash
COPY pipe /
RUN chmod a+x /*.sh
ENTRYPOINT ["/pipe.sh"]