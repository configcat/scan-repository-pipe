# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.8.0

- minor: Use ConfigCat CLI 2.4.0

## 1.7.1

- patch: Use ConfigCat CLI 2.3.2

## 1.7.0

- minor: Use ConfigCat CLI 2.3.1

## 1.6.0

- minor: Use ConfigCat CLI 2.2.2 / Add USAGE_PATTERNS input variable handling.

## 1.5.0

- minor: Use ConfigCat CLI 2.1.0 / Add ALIAS_PATTERNS input variable handling.

## 1.4.4

- patch: Execute git config set in git repo folder

## 1.4.3

- patch: Fix failing active branch tracking

## 1.4.2

- patch: Fix folder dubious ownership issue

## 1.4.1

- patch: Fix failing active branch tracking

## 1.4.0

- minor: Use latest ConfigCat CLI

## 1.3.0

- minor: Use latest CLI, add EXCLUDE_KEYS input argument.

## 1.2.1

- patch: Fix git folder ownership issue.

## 1.2.0

- minor: Use ConfigCat CLI 1.6.0

## 1.1.2

- patch: Set the correct runner version.

## 1.1.1

- patch: Use 4 as default context line count.

## 1.1.0

- minor: Use newer ConfigCat CLI, fix file template urls.

## 1.0.0

- major: Add required parameter validation.

